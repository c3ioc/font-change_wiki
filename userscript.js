const fontSwitcherCSS = `
/* font converted using font-converter.net. thank you! */
@font-face {
  font-family: "SylexiadSansMedium-Bold";
  src: url("lib/plugins/fontface/fonts/SylexiadSansMedium-Bold.eot");
  /* IE9 Compat Modes */
  src: url("lib/plugins/fontface/fonts/SylexiadSansMedium-Bold.eot?#iefix") format("embedded-opentype"),
  url("lib/plugins/fontface/fonts/SylexiadSansMedium-Bold.otf") format("opentype"),
  url("lib/plugins/fontface/fonts/SylexiadSansMedium-Bold.svg") format("svg"),
  url("lib/plugins/fontface/fonts/SylexiadSansMedium-Bold.ttf") format("truetype"),
  url("lib/plugins/fontface/fonts/SylexiadSansMedium-Bold.woff") format("woff"),
  url("lib/plugins/fontface/fonts/SylexiadSansMedium-Bold.woff2") format("woff2");
  /* Modern Browsers */
  font-weight: normal;
  font-style: normal;
  font-display: swap;
}
@font-face {
  font-family: "SylexiadSansMedium";
  src: url("lib/plugins/fontface/fonts/SylexiadSansMedium.eot");
  /* IE9 Compat Modes */
  src: url("lib/plugins/fontface/fonts/SylexiadSansMedium.eot?#iefix") format("embedded-opentype"),
  url("lib/plugins/fontface/fonts/SylexiadSansMedium.otf") format("opentype"),
  url("lib/plugins/fontface/fonts/SylexiadSansMedium.svg") format("svg"),
  url("lib/plugins/fontface/fonts/SylexiadSansMedium.ttf") format("truetype"),
  url("lib/plugins/fontface/fonts/SylexiadSansMedium.woff") format("woff"),
  url("lib/plugins/fontface/fonts/SylexiadSansMedium.woff2") format("woff2");
  /* Modern Browsers */
  font-weight: normal;
  font-style: normal;
  font-display: swap;
}
@font-face {
  font-family: "AtkinsonHyperlegible-Bold";
  src: url("lib/plugins/fontface/fonts/Atkinson-Hyperlegible-Bold.eot");
  /* IE9 Compat Modes */
  src: url("lib/plugins/fontface/fonts/Atkinson-Hyperlegible-Bold.eot?#iefix") format("embedded-opentype"),
  url("lib/plugins/fontface/fonts/Atkinson-Hyperlegible-Bold.otf") format("opentype"),
  url("lib/plugins/fontface/fonts/Atkinson-Hyperlegible-Bold.svg") format("svg"),
  url("lib/plugins/fontface/fonts/Atkinson-Hyperlegible-Bold.ttf") format("truetype"),
  url("lib/plugins/fontface/fonts/Atkinson-Hyperlegible-Bold.woff") format("woff"),
  url("lib/plugins/fontface/fonts/Atkinson-Hyperlegible-Bold.woff2") format("woff2");
  /* Modern Browsers */
  font-weight: normal;
  font-style: normal;
  font-display: swap;
}
@font-face {
  font-family: "AtkinsonHyperlegible-Regular";
  src: url("lib/plugins/fontface/fonts/Atkinson-Hyperlegible-Regular.eot");
  /* IE9 Compat Modes */
  src: url("lib/plugins/fontface/fonts/Atkinson-Hyperlegible-Regular.eot?#iefix") format("embedded-opentype"),
  url("lib/plugins/fontface/fonts/Atkinson-Hyperlegible-Regular.otf") format("opentype"),
  url("lib/plugins/fontface/fonts/Atkinson-Hyperlegible-Regular.svg") format("svg"),
  url("lib/plugins/fontface/fonts/Atkinson-Hyperlegible-Regular.ttf") format("truetype"),
  url("lib/plugins/fontface/fonts/Atkinson-Hyperlegible-Regular.woff") format("woff"),
  url("lib/plugins/fontface/fonts/Atkinson-Hyperlegible-Regular.woff2") format("woff2");
  /* Modern Browsers */
  font-weight: normal;
  font-style: normal;
  font-display: swap;
}
@font-face {
  font-family: "OpenDyslexicThree-Bold";
  src: url("lib/plugins/fontface/fonts/OpenDyslexic3-Bold.eot");
  /* IE9 Compat Modes */
  src: url("lib/plugins/fontface/fonts/OpenDyslexic3-Bold.eot?#iefix") format("embedded-opentype"),
  url("lib/plugins/fontface/fonts/OpenDyslexic3-Bold.otf") format("opentype"),
  url("lib/plugins/fontface/fonts/OpenDyslexic3-Bold.svg") format("svg"),
  url("lib/plugins/fontface/fonts/OpenDyslexic3-Bold.ttf") format("truetype"),
  url("lib/plugins/fontface/fonts/OpenDyslexic3-Bold.woff") format("woff"),
  url("lib/plugins/fontface/fonts/OpenDyslexic3-Bold.woff2") format("woff2");
  /* Modern Browsers */
  font-weight: normal;
  font-style: normal;
  font-display: swap;
}
@font-face {
  font-family: "OpenDyslexicThree-Regular";
  src: url("lib/plugins/fontface/fonts/OpenDyslexic3-Regular.eot");
  /* IE9 Compat Modes */
  src: url("lib/plugins/fontface/fonts/OpenDyslexic3-Regular.eot?#iefix") format("embedded-opentype"),
  url("lib/plugins/fontface/fonts/OpenDyslexic3-Regular.otf") format("opentype"),
  url("lib/plugins/fontface/fonts/OpenDyslexic3-Regular.svg") format("svg"),
  url("lib/plugins/fontface/fonts/OpenDyslexic3-Regular.ttf") format("truetype"),
  url("lib/plugins/fontface/fonts/OpenDyslexic3-Regular.woff") format("woff"),
  url("lib/plugins/fontface/fonts/OpenDyslexic3-Regular.woff2") format("woff2");
  /* Modern Browsers */
  font-weight: normal;
  font-style: normal;
  font-display: swap;
}

.font-SylexiadSans {
  font-family: SylexiadSansMedium, sans-serif !important;
  font-size: 13.8pt;
}
.font-SylexiadSans h1,
.font-SylexiadSans h2,
.font-SylexiadSans h3,
.font-SylexiadSans h4,
.font-SylexiadSans h5,
.font-SylexiadSans h6 {
  font-family: SylexiadSansMedium-Bold, sans-serif !important;
}

.font-AtkinsonHyperlegible {
  font-family: AtkinsonHyperlegible-Regular, sans-serif !important;
  font-size: 12.3pt;
}
.font-AtkinsonHyperlegible h1,
.font-AtkinsonHyperlegible h2,
.font-AtkinsonHyperlegible h3,
.font-AtkinsonHyperlegible h4,
.font-AtkinsonHyperlegible h5,
.font-AtkinsonHyperlegible h6 {
  font-family: AtkinsonHyperlegible-Bold, sans-serif !important;
}

.font-OpenDyslexicThree {
  font-family: OpenDyslexicThree-Regular, sans-serif !important;
  font-size: 11pt;
}
.font-OpenDyslexicThree h1,
.font-OpenDyslexicThree h2,
.font-OpenDyslexicThree h3,
.font-OpenDyslexicThree h4,
.font-OpenDyslexicThree h5,
.font-OpenDyslexicThree h6 {
  font-family: OpenDyslexicThree-Bold, sans-serif !important;
}

.font-serif {
  font-family: serif !important;
  font-size: 13pt;
}
.font-serif h1,
.font-serif h2,
.font-serif h3,
.font-serif h4,
.font-serif h5,
.font-serif h6 {
  font-family: serif !important;
  font-weight: bold;
}

.font-sans {
  font-family: sans-serif !important;
  font-size: 12.3pt;
}
.font-sans h1,
.font-sans h2,
.font-sans h3,
.font-sans h4,
.font-sans h5,
.font-sans h6 {
  font-family: sans-serif !important;
  font-weight: bold;
}

/* Utility classes */
.display-none {
  display: none;
}

/* Font list styling */
#accessibility-options button {
  float: right;
  color: currentColor;
  background: inherit;
  border-color: currentColor;
  transition: background-color .15s ease-in;
  padding: .35em .7em;
  margin-top: .7em;
}

#accessibility-options button:hover, button:active {
  filter: brightness(1.2);
}

#font-selection{
  margin-top: 1em;
  width: 16rem; /* old browsers */
  width: max-content;
  float: right;
  border-color: currentColor;
  clear: both;
}
#font-selection legend {
  padding: 0 0.7rem;
}
#font-selection ul {
  display: flex;
  flex-direction: column;
}
#font-selection ul li {
  display: block;
  text-align: left;
  height: 1.75rem;
  margin-right: 0.8rem;
}
#font-selection input[type="radio"] {
  margin-right: 10px;
}
@media only screen and (max-width: 480px){
  #accessibility-options button{
    float: none;
  }
  #font-selection{
    float: none;
  }
}


`;

const fontSwitcherHTML = `
<div class="js no-print" lang="en" id="accessibility-options">
<button class="no-print" type="button">
  Accessibility&nbsp;Options
</button>
<fieldset class="display-none" id="font-selection">
  <legend>Font</legend>

  <ul>
    <li>
      <label for="font-default">
        <input
          type="radio"
          name="font"
          id="font-default"
          value="default"
          checked
        />Default
      </label>
    </li>
    <li>
      <label
        for="font-AtkinsonHyperlegible"
        class="font-AtkinsonHyperlegible"
      >
        <input
          type="radio"
          name="font"
          id="font-AtkinsonHyperlegible"
          value="AtkinsonHyperlegible"
        />Atkinson&nbsp;Hyperlegible</label
      >
    </li>
    <li>
      <label
        for="font-OpenDyslexicThree"
        class="font-OpenDyslexicThree"
      >
        <input
          type="radio"
          name="font"
          id="font-OpenDyslexicThree"
          value="OpenDyslexicThree"
        />Open&nbsp;Dyslexic</label
      >
    </li>
    <li>
      <label for="font-SylexiadSans" class="font-SylexiadSans">
        <input
          type="radio"
          name="font"
          id="font-SylexiadSans"
          value="SylexiadSans"
        />Sylexiad&nbsp;Sans</label
      >
    </li>
    <li>
      <label for="font-serif" class="font-serif"
        ><input
          type="radio"
          name="font"
          id="font-serif"
          value="serif"
        />serif</label
      >
    </li>
    <li>
      <label for="font-sans" class="font-sans">
        <input
          type="radio"
          name="font"
          id="font-sans"
          value="sans"
        />sans-serif</label
      >
    </li>
  </ul>
</fieldset>
</div>
`;

// toggle "Accessibility Options" menu
const showAccessibilityMenu = () => {
  const fontSelection = document.getElementById("font-selection");
  if (fontSelection.classList.contains("display-none")) {
    fontSelection.classList.remove("display-none");
  } else {
    fontSelection.classList.add("display-none");
  }
};

// set font
const changeFont = event => {
  console.log("You selected: ", event.target.value);
  const font = event.target.value;
  if (!font) return;
  const contentNode = document.getElementsByTagName("body")[0];
  if (!contentNode) return;
  contentNode.classList.remove(
    "font-SylexiadSans",
    "font-AtkinsonHyperlegible",
    "font-OpenDyslexicThree",
    "font-serif",
    "font-sans"
  );
  if (font === "default") {
    window.localStorage.removeItem("font");
  } else {
    contentNode.classList.add(`font-${font}`);
    window.localStorage.setItem("font", font);
  }
};

// get font from localStorage
window.onload = function() {
  const previousFont = localStorage.getItem("font");
  if (!previousFont) {
    return;
  }

  const contentNode = document.getElementsByTagName("body")[0];
  contentNode.classList.remove(
    "font-SylexiadSans",
    "font-AtkinsonHyperlegible",
    "font-OpenDyslexicThree",
    "font-serif",
    "font-sans"
  );
  contentNode.classList.add(`font-${previousFont}`);
  console.log("Previously selected font: ", previousFont);
  radiobtn = document.getElementById(`font-${previousFont}`);
  radiobtn.checked = true;
};

/* Add the font switcher HTML to the page */
const fontSwitcherHTMLTag = document.createElement("div");
fontSwitcherHTMLTag.innerHTML = fontSwitcherHTML;
document.getElementById("dokuwiki__sitetools").appendChild(fontSwitcherHTMLTag);

/* Add the CSS to the head */
const fontSwitcherCSSTag = document.createElement("style");
fontSwitcherCSSTag.innerHTML = fontSwitcherCSS;
document.head.appendChild(fontSwitcherCSSTag);

/* Set up event listeners */
document
  .querySelector("#accessibility-options button")
  .addEventListener("click", showAccessibilityMenu);
document
  .getElementById("font-selection")
  .addEventListener("change", changeFont);
