# Font-change for DokuWiki

This is code to add a font change button to a DokuWiki, which we created because some people with visual impairments or dyslexia prefer those fonts.

## Howto
1. Move the fonts in ‘lib/plugins/fontface/fonts/‘ into the folder where you store the fonts for your wiki, in our case that is ‘lib/plugins/fontface/fonts/‘
2. If you store the fonts elsewhere, go to the @font-face declarations in userscript.js and change the URLs to the fonts accordingly 
3. Copy the code from userscript.js into conf/userstyle.js (or create it if absent)
4. If your browser uses JavaScript, an ‘Accessibility Options’ button should appear in the header of your wiki. Enjoy.