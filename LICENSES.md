userscript.js is licensed as [CC0](https://creativecommons.org/publicdomain/zero/1.0/legalcode)

Open Dyslexic is licensed as [SIL Open Font License 1.1](https://github.com/antijingoist/opendyslexic/blob/master/OFL.txt)

Atkinson Hyperlegible is licensed as [Atkinson Hyperlegible Font License](ttps://www.brailleinstitute.org/wp-content/uploads/2020/11/Atkinson-Hyperlegible-Font-License-2020-1104.pdf)

We aren’t sure about the license of Sylexiad Sans since the author didn’t supply one or answered our mail. We got it from [sylexiad.com](https://www.sylexiad.com).